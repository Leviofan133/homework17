﻿#include <iostream>

class Animal 
{
public:
    virtual void Voice() 
    {
        std::cout << "Animal voice";
    }
};

class Dog : public Animal 
{
public:
    void Voice() override 
    {
        std::cout << "Woof! \n";
    }
};

class Cat : public Animal 
{
public:
    void Voice() override 
    {
        std::cout << "Meow! \n";
    }
};

class Chick : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Sheep-sheep! \n";
    }
};

class Hog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Grunt-grunt! \n";
    }
};

class Cow : public Animal 
{
public:
    void Voice() override 
    {
        std::cout << "Moo!";
    }
};


int main() 
{
    Animal* Animals[5];
    Animals[0] = new Dog();
    Animals[1] = new Cat();
    Animals[2] = new Chick();
    Animals[3] = new Hog();
    Animals[4] = new Cow();

    for (int i = 0; i < 5; i++) 
    {
        Animals[i]->Voice();
    }

    for (int i = 0; i < 5; i++) 
    {
        delete Animals[i];
    }

    return 0;
}
